<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Remote Camera</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css" crossorigin="anonymous">
    <!-- Lightbox2 CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.min.css" rel="stylesheet">

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Remote Camera control</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto" id="settings-menu">

        </ul>
    </div>
</nav>

<main role="main">
    <div class="container">
        <div class="jumbotron">
            <h1>{{$camera}} <small>{{$charge}}</small></h1>
            <hr>
            <div class="row">
                <div class="col-md-8">
                    @include('shutter.video_feed')
                </div>
                <div class="col-md-4">
                    <a href="#" class="btn btn-primary btn-lg d-block mx-auto mt-4" id="take_picture_btn">Fotó készítése</a>
                    <hr>
                    <p>Legutóbb elkészített kép</p>
                    <a id="latest_image_link" href="" data-lightbox="gallery" data-title="Latest Image">
                        <img id="latest_image" src="" class="img-thumbnail" alt="Responsive image">
                    </a>
                </div>
            </div>
            <div id="previous_pictures" class="row">

            </div>
        </div>


    </div>
</main>
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="/js/bootstrap.bundle.js" ></script>
<script src="/js/popper.js"></script>

<!-- Lightbox2 JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox-plus-jquery.min.js"></script>
<script>
    $(document).ready(function() {
        // Load images when the page loads
        loadImages();

        // Take picture button click event
        $('#settingControlModal').on('hidden.bs.modal', function () {
            $('#settingControlModal .modal-body').html('Beállítás beolvasása');
        });

        $('#take_picture_btn').click(function(e) {
            e.preventDefault();
            $.get('/api/camera/shutter', function(data) {
                // Move current latest image to previous pictures
                var latestImgSrc = $('#latest_image').attr('src');
                if (latestImgSrc) {
                    $('#previous_pictures').prepend(
                        '<div class="col-md-3 mb-2">' +
                        '<a href="' + latestImgSrc + '" data-lightbox="gallery" data-title="Previous Image">' +
                        '<img src="' + latestImgSrc + '" class="img-thumbnail">' +
                        '</a>' +
                        '</div>'
                    );
                }

                // Set the new latest image
                $('#latest_image').attr('src', data.thumbnail);
                $('#latest_image_link').attr('href', data.thumbnail);
            });
        });

        $.ajax({
            url: '/api/camera/settings',
            type: 'GET',
            success: function(data) {
                // Create menu items and modals for each group
                $.each(data, function(groupName, settings) {
                    // Create menu item
                    $('#settings-menu').append(`<li class="nav-item"><a class="nav-link" href="#" data-toggle="modal" data-target="#modal-${groupName}">${groupName}</a></li>`);

                    // Create modal
                    let modalContent = settings.map(setting => `<button type="button" class="list-group-item list-group-item-action setting-btn" data-toggle="modal" data-target="#settingControlModal" data-setting="${setting}">${setting}</button>`).join('');
                    $('body').append(`
                    <div class="modal fade first-modal" id="modal-${groupName}" tabindex="-1" role="dialog" aria-labelledby="${groupName}Label" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="${groupName}Label">${groupName}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="list-group">${modalContent}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                `);
                });
                $('.setting-btn').on('click', function() {
                    var settingName = $(this).data('setting'); // Assuming you have data-setting attribute with the setting name
                    getCameraSetting(settingName);

                });

            },
            error: function(error) {
                console.log(error);
            }
        });
    });

    function loadImages() {
        $.get('/api/camera/images', function(data) {
            if (data.length > 0) {
                // Set the latest image
                $('#latest_image_link').attr('href', data[0]);
                $('#latest_image').attr('src', data[0]);

                // Append other images to previous pictures
                for (var i = 1; i < data.length; i++) {
                    $('#previous_pictures').append(
                        '<div class="col-md-3 mb-2">' +
                        '<a href="' + data[i] + '" data-lightbox="gallery" data-title="Previous Image">' +
                        '<img src="' + data[i] + '" class="img-thumbnail">' +
                        '</a>' +
                        '</div>'
                    );
                }
            }
        });
    }

    function getCameraSetting(settingName) {
        $('#settingControlModal .modal-body').html('Beállítás beolvasása');
        $.ajax({
            url: 'api/camera/setting/' + settingName,
            success: function(response) {
                // Set the modal title
                $('#settingsModalLabel').text(settingName + ' Settings');

                // Generate the form based on the response
                var formContent = '';
                switch (response.type) {
                    case 'TEXT':
                        formContent = '<input type="text" class="form-control" value="' + response.value + '"' + (response.readonly !== '0' ? ' disabled' : '') + '>';
                        break;
                    case 'TOGGLE':
                        formContent = '<input type="checkbox" class="form-check-input"' + (response.value === 'On' ? ' checked' : '') + (response.readonly !== '0' ? ' disabled' : '') + '>';
                        break;
                    case 'DATE':
                        formContent = '<input type="date" class="form-control" value="' + response.value + '"' + (response.readonly !== '0' ? ' disabled' : '') + '>';
                        break;
                    case 'TIME':
                        formContent = '<input type="time" class="form-control" value="' + response.value + '"' + (response.readonly !== '0' ? ' disabled' : '') + '>';
                        break;
                    case 'NUMBER':
                        formContent = '<input type="number" class="form-control" value="' + response.value + '"' + (response.readonly !== '0' ? ' disabled' : '') + '>';
                        break;
                    case 'RANGE':
                        formContent = '<input type="range" class="form-range" value="' + response.value + '"' + (response.readonly !== '0' ? ' disabled' : '') + '>';
                        break;
                    case 'RADIO':
                        if (Array.isArray(response.choices)) {
                            formContent = response.choices.map(function(choice, index) {
                                return '<div class="form-check">' +
                                    '<input class="form-check-input" type="radio" name="' + settingName + 'RadioOptions" id="' + settingName + 'Radio' + index + '"' +
                                    ' value="' + choice + '"' + (response.value === choice ? ' checked' : '') + (response.readonly !== '0' ? ' disabled' : '') + '>' +
                                    '<label class="form-check-label" for="' + settingName + 'Radio' + index + '">' +
                                    choice +
                                    '</label>' +
                                    '</div>';
                            }).join('');
                        }
                        break;
                    default:
                        console.log('Unhandled type: ' + response.type);
                        console.log(response);
                }
                // Load the generated form content into the modal body
                $('#settingControlModal .modal-body').html(formContent);
            }
        });
    }

    function saveCameraSetting(settingName, newValue, callback) {
        $.ajax({
            url: '/api/camera/setting',
            type: 'POST',
            data: {
                setting: settingName,
                value: newValue
            },
            success: function(data) {
                callback(data);
            },
            error: function(error) {
                console.log(error);
            }
        });
    }


</script>
@include("modals.basic_config")
@include("modals.setting_modal")
</body>