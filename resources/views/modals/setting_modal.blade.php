<!-- Settings Modal -->
<div class="modal fade second-modal" id="settingControlModal" tabindex="-1" role="dialog" aria-labelledby="settingControlModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="settingControlModalLabel">Setting</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Beállítás beolvasása
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="saveSetting">Save changes</button>
            </div>
        </div>
    </div>
</div>
