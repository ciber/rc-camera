<!-- ISO Settings Modal -->
<div class="modal fade" id="basicConfigModal" tabindex="-1" role="dialog" aria-labelledby="basicConfigModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="basicConfigModalLabel">ISO Settings</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- ISO Settings Form -->
                <form id="isoSettingsForm">
                    <div class="form-group">
                        <label for="isoSetting">ISO</label>
                        <select class="form-control" id="isoSetting">
                            <option>Auto</option>
                            <option>100</option>
                            <option>200</option>
                            <option>400</option>
                            <option>800</option>
                            <!-- Add more ISO options as needed -->
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="saveIsoSettings">Save changes</button>
            </div>
        </div>
    </div>
</div>
