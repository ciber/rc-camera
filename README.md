# Camera Control System

This project provides a web interface for controlling a camera connected via USB, including capturing images, adjusting settings, and live video feed. It utilizes `gphoto2` for camera control and an Nginx server with HLS (HTTP Live Streaming) for video streaming.

## Prerequisites

Before you start, make sure you have the following installed:
- [gphoto2](http://gphoto.org/)
- [Nginx](https://nginx.org/) with HLS module for video playback

## Repository Structure

- `bin/` - Contains the `camerashutter` bash script to trigger the camera shutter.
- `src/` - Contains the source code for the web interface.
- `vendor/` - Contains the dependencies managed by Composer.

## Installation

1. Clone the repository to your local machine or server:

    ```
    git clone https://github.com/ciber/rc-camera.git
    ```

2. Navigate to the project directory:

    ```
    cd rc-camera
    ```

3. Install the PHP dependencies using [Composer](https://getcomposer.org/):

    ```
    composer install
    ```

## Usage

1. Start the Nginx server with HLS module enabled.

2. Run the PHP built-in server (or use a PHP server of your choice):

    ```
    php -S localhost:8000
    ```

3. Access the web interface through your web browser:

    ```
    http://localhost:8000
    ```

4. Use the web interface to control your camera.

---

## Camera Shutter Script

The `camerashutter` script is a simple bash script that triggers the camera shutter using `gphoto2`. This script should be placed in a directory that's in your system's PATH to allow easy execution from any location.

To move the script to `/usr/local/bin` and make it executable, follow these steps:

1. Navigate to the `bin/` directory in the project:

    ```bash
    cd rc-camera/bin/
    ```

2. Move the `camerashutter` script to `/usr/local/bin`:

    ```bash
    sudo mv camerashutter /usr/local/bin/
    ```

3. Make the script executable:

    ```bash
    sudo chmod +x /usr/local/bin/camerashutter
    ```

Now, you can run the `camerashutter` command from anywhere in the terminal to trigger the camera shutter.

---

This should provide a clear guide on how to make the script globally accessible and executable. Adjust any paths or names according to your project's structure and naming conventions.

## Contributing

We welcome contributions to this project. Please fork the repository and submit pull requests with your changes.

## License

This project is licensed under the [MIT License](LICENSE).