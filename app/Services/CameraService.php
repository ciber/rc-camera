<?php

// app/Services/CameraService.php
namespace App\Services;

use Illuminate\Support\Facades\Request;

class CameraService
{
    public function getCameraType()
    {
        $output = [];
        $return_var = 0;
        exec('gphoto2 --auto-detect', $output, $return_var);
        if ($return_var !== 0) {
            return null;
        }

        foreach ($output as $line) {
            if (!str_starts_with($line, 'Model') && !str_starts_with($line, '----') && trim($line) !== '') {
                $parts = preg_split('/\s{2,}/', $line);
                return $parts[0];
            }
        }

        return null;
    }

    public function getCameraSettings()
    {
        $output = [];
        $return_var = 0;
        exec('gphoto2 --list-config', $output, $return_var);
        if ($return_var !== 0) {
            // Error handling if the exec command fails
            return null;
        }

        $settings = [];
        foreach ($output as $line) {
            if (preg_match('/\/main\/(.+)\/(.+)/', $line, $matches)) {
                $settingType = $matches[1];
                $settingFunction = $matches[2];
                $settings[$settingFunction] = $settingType;
            }
        }

        return $settings;
    }

    public function getCameraSettingsGrouped()
    {
        $output = [];
        $return_var = 0;
        exec('gphoto2 --list-config', $output, $return_var);

        if ($return_var !== 0) {
            // Error handling if the exec command fails
            return null;
        }

        $settingsGrouped = [];
        foreach ($output as $line) {
            if (preg_match('/\/main\/(.+)\/(.+)/', $line, $matches)) {
                $settingType = $matches[1];
                $settingFunction = $matches[2];
                // Initialize the array if it doesn't exist
                if (!isset($settingsGrouped[$settingType])) {
                    $settingsGrouped[$settingType] = [];
                }
                // Append the setting function to the appropriate group
                $settingsGrouped[$settingType][] = $settingFunction;
            }
        }

        return $settingsGrouped;
    }

    public function getCameraSetting($setting){
        $settings = $this->getCameraSettings();
        if (isset($settings[$setting])) {
            $settingType = $settings[$setting];
            $configPath = "/main/{$settingType}/{$setting}";
            $command = "gphoto2 --get-config {$configPath}";
            exec($command, $output, $return_var);

            if ($return_var === 0 && !empty($output)) {
                $parsedOutput = [];
                foreach ($output as $line) {
                    if (preg_match('/^([^:]+): (.+)/', $line, $matches)) {
                        $key = strtolower($matches[1]);
                        $value = $matches[2];
                        $parsedOutput[$key] = $value;
                    }
                }

                if (isset($parsedOutput['type']) && isset($parsedOutput['current'])) {
                    $data =  [
                        'type' => $parsedOutput['type'],
                        'value' => $parsedOutput['current'],
                        'readonly' => $parsedOutput['readonly']
                    ];
                    if(isset($parsedOutput['choice'])){
                        $data['choices'] = explode(" ", $parsedOutput["choice"]);
                    }
                    return $data;
                }
            }
        }
        return null;
    }


    public function setCameraSetting($settingFunction, $newValue)
    {
        $settings = $this->getCameraSettings();
        if (isset($settings[$settingFunction])) {
            $settingType = $settings[$settingFunction];
            $configPath = "/main/{$settingType}/{$settingFunction}";
            $command = "gphoto2 --set-config {$configPath}={$newValue}";
            exec($command, $output, $return_var);

            if ($return_var !== 0) {
                // Error handling if the exec command fails
                return false;
            }
            return true;
        }

        return false;
    }


}
