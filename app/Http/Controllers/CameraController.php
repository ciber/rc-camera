<?php

namespace App\Http\Controllers;

use App\Facades\CameraFacade;
use App\Services\CameraService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class CameraController extends Controller
{
    public function mainControl(){
        $camera = CameraFacade::getCameraType();
        $battery = CameraFacade::getCameraSetting('batterylevel');
        return view("shutter.remote-eye", [
            "camera" => $camera ?? "Nem található kamera",
            "charge" => $battery ? $battery["value"] : 0
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function captureImage()
    {
        $file = exec("camerashutter ".public_path("camera"), $output);
        return new JsonResponse(["thumbnail" => asset("camera/".$file)]);
    }

    /**
     * @return JsonResponse
     */
    public function capturedImages(){
        $imageFiles = File::files(public_path("camera"));
        usort($imageFiles, function($a, $b) {
            return -strcmp($a->getFilename(), $b->getFilename());
        });
        $imagePaths = array_map(function($file) {
            return asset("camera/" . $file->getFilename());
        }, $imageFiles);
        return new JsonResponse($imagePaths);
    }

    public function cameraSettings(){
        if (CameraFacade::getCameraType() === null) {
            return response()->json(['error' => 'No camera connected'], 404);
        }
        return CameraFacade::getCameraSettingsGrouped();
    }

    public function saveCameraSetting(Request $request)
    {
        $setting = $request->input('setting');
        $value = $request->input('value');

        // Implement logic to save a specific camera setting
        // ...

        return response()->json(['message' => 'Setting saved successfully']);
    }

    public function getCameraSetting($setting)
    {
        return new JsonResponse(CameraFacade::getCameraSetting($setting));
    }
}