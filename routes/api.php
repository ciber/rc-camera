<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/camera/shutter', 'App\Http\Controllers\CameraController@captureImage')->name('camerashutter');
Route::get('/camera/images', 'App\Http\Controllers\CameraController@capturedImages')->name('camera_thumbnails');
Route::get('/camera/settings', 'App\Http\Controllers\CameraController@cameraSettings')->name('camera_settings');

Route::get('/camera/setting/{setting}', 'App\Http\Controllers\CameraController@getCameraSetting');
Route::post('/camera/setting', 'App\Http\Controllers\CameraController@saveCameraSetting');